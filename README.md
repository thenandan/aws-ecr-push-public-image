# Bitbucket Pipelines Pipe: AWS ECR push image

Pushes docker images to the AWS Elastic Container Registry (Public).
This repository is forked from [Bitbucket aws-ecr-push-image](https://bitbucket.org/atlassian/aws-ecr-push-image/src/master/) repository.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:
```yaml
- pipe: thenandan/aws-ecr-push-public-image:1.2.2
  variables:
    AWS_ACCESS_KEY_ID: '<string>' # Optional if already defined in the context.
    AWS_SECRET_ACCESS_KEY: '<string>' # Optional if already defined in the context.
    AWS_DEFAULT_REGION: 'us-east-1' # Fixed. Currently aws supports only one region
    AWS_ECR_VISIBILITY: 'public|private' # Fixed, based on the repository type 
    AWS_ECR_PUBLIC_KEY: 'string' # Required, you account public key for ECR. 
    IMAGE_NAME: "<string>"
    # TAGS: "<string>" # Optional
    # DEBUG: "<boolean>" # Optional
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| AWS_ACCESS_KEY_ID (**) | AWS access key id |
| AWS_SECRET_ACCESS_KEY (**) | AWS secret key |
| AWS_DEFAULT_REGION (**) | AWS region (`us-east-1` for public repository) |
| AWS_ECR_VISIBILITY (*)  | `public` or `private` |
| AWS_ECR_PUBLIC_KEY   | Required only for pushing the image into public repository. You can find this key in the public repository url eg. public.ecr.aws/`key`/repository_name:latest. See the attached file below.  |
| IMAGE_NAME            | The name of the image to push to the ECR. The name should be the same as your ECR repository name. Remember that you don't need to add your registry URL in front of the image name, the pipe will fetch this URL from AWS and add it to the image tag for you.|
| TAGS                  | List of white space separated tags to push. Default: `latest`.|
| DEBUG                 | Turn on extra debug information. Default: `false`. |
_(*) = required variable. This variable needs to be specified always when using the pipe._
_(**) = required variable. If this variable is configured as a repository, account or environment variable, it doesn’t need to be declared in the pipe as it will be taken from the context. It can still be overridden when using the pipe._


## Prerequisites

To use this pipe you should have a IAM user configured with programmatic access, with the necessary permissions to push docker images to your ECR repository. You also need to set up a
ECR container registry if you don't already have on. Here is a [AWS ECR Getting started](https://docs.aws.amazon.com/AmazonECR/latest/userguide/ECR_GetStarted.html) guide from AWS on how to set up a new registry.

To use the pipe for pushing the image into public repository, IAM user must have the `ecr-public:GetAuthorizationToken` and `sts:GetServiceBearerToken` permissions.
Here us a [AmazonECRPublic API Reference](https://docs.aws.amazon.com/AmazonECRPublic/latest/APIReference/Welcome.html) guide from AWS.

IMPORTANT! This pipe expects the docker image to be built already. You can see the examples below for more details.

Note - Rest you can follow the [Bitbucket documentation](https://bitbucket.org/atlassian/aws-ecr-push-image/src/master/) for pushing the docker image into private ECR repository.

[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,aws,ecr
